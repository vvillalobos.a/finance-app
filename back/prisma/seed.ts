import { PrismaClient } from '@prisma/client'
import * as bcrypt from 'bcrypt'

const prisma = new PrismaClient()
async function main() {

    const password = await bcrypt.hash('password', 10)

    const alice = await prisma.user.upsert({
        where: { email: 'alice@example.com' },
        update: {},
        create: {
            email: 'alice@example.com',
            name: 'Alice',
            password: password,
            accounts: {
                create: {
                    name: 'Bank A Checking Account',
                    balance: 0,
                    currency: 'USD'
                },
            },
        },
    })

    const bob = await prisma.user.upsert({
        where: { email: 'bob@example.com' },
        update: {},
        create: {
            email: 'bob@example.com',
            name: 'Bob',
            password: password,
            accounts: {
                create: {
                    name: 'Bank A Checking Account',
                    balance: 0,
                    currency: 'USD'
                },
            },
        },
    });

    [alice, bob].forEach(async user => {

        const accounts = await prisma.bankAccount.findMany({
            where: { ownerId: user.id }
        });

        accounts.forEach(account => {

            const exampleTransactions = [
                {
                    title: 'Initial Deposit',
                    amount: 1000,
                    accountId: account.id,
                    date: new Date()
                },
                {
                    title: 'Rent',
                    amount: -500,
                    accountId: account.id,
                    date: new Date()
                },
                {
                    title: 'Groceries',
                    amount: -100,
                    accountId: account.id,
                    date: new Date()
                },
                {
                    title: 'Paycheck',
                    amount: 2000,
                    accountId: account.id,
                    date: new Date()
                }
            ]

            exampleTransactions.forEach(async transaction => {

                await prisma.transaction.create({ data: transaction })

            });

        });

    });

    console.log("")
    console.log("✨ The following users have been created:")
    console.table([alice, bob], ['id', 'name', 'email'])
    console.log("")
    console.log("🔑 The password for all generated users is 'password'")
}
main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })