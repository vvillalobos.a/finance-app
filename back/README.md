# Finance App - Back end

This is the back end of a finance app that allows users to track their expenses and income. It is built using NestJS and Prisma.

This readme is specific to the back end. For a more holistic view of the project, please refer to the [main README](../README.md).

## Project Overview

This project is a finance app that allows users to track their expenses and income. It is built using NestJS and Prisma. The app is made for my personal use and is not yet ready for deployment. It is still in development and may not be stable.

## Features
- Prisma ORM for database management and migrations
- NestJS for the API
- Swagger plugin for API documentation
- Faker for seeding the database with test data

## Development
Follow the instructions in the main README to set up the development environment.

To know more about the framework and tools used in this project, please refer to the resources below.

- NestJS: [https://nestjs.com/](https://nestjs.com/)
- Prisma: [https://www.prisma.io/](https://www.prisma.io/)
- Swagger: [https://swagger.io/](https://swagger.io/)

For development, I recommend using a large expiration time for the 
JWT tokens. You can set this in the `back/.env` file (or `back/.env.template` if you haven't renamed it).

VSCode + Thunder client is a great combination for testing the API endpoints.

## Usage

The NestJS app is run using Docker and Docker Compose. Once the `make up` command is executed, the app will be available at `http://localhost:4000`.

The Swagger documentation is available at `http://localhost:4000/api`.