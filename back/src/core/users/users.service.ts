import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/core/prisma/prisma.service";
import { CreditCard, Prisma } from "@prisma/client";
import * as bcrypt from 'bcrypt';
import { CreateUserDto } from "./dto/createuser.dto";
import { UpdateUserDto } from "./dto/updateuser.dto";

export const hashingRounds = 10;

@Injectable()
export class UsersService {
    constructor(private prisma: PrismaService) { }

    async findOne(userId: number) {
        return this.prisma.user.findUnique({
            where: {
                id: userId
            }
        });
    }

    async findAll() {
        return this.prisma.user.findMany();
    }

    async create(createUserDto: CreateUserDto) {

        const hashedPassword = await bcrypt.hash(
            createUserDto.password,
            hashingRounds
        );

        createUserDto.password = hashedPassword;

        return this.prisma.user.create({
            data: createUserDto
        });
    }

    async update(userId: number, updateUserDto: UpdateUserDto) {

        if (updateUserDto.password) {
            updateUserDto.password = await bcrypt.hash(
                updateUserDto.password,
                hashingRounds
            );
        }

        return this.prisma.user.update({
            where: {
                id: userId
            },
            data: updateUserDto
        });
    }

    remove(id: number) {
        return this.prisma.user.delete({
            where: {
                id
            }
        });
    }
}