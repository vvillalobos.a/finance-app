import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { JwtService } from '@nestjs/jwt';
import { AuthenticationEntity } from './entity/authentication.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthenticationService {
    constructor(private prisma: PrismaService, private jwtService: JwtService) { }

    async login(email: string, password: string): Promise<AuthenticationEntity> {

        const user = await this.prisma.user.findUnique({
            where: { email }
        })

        if (!user) {
            // We don't want to give away if the email is valid or not
            throw new UnauthorizedException('invalid_credentials');
        }

        const isPasswordValid = await bcrypt.compare(password, user.password);

        if (!isPasswordValid) {
            throw new UnauthorizedException('invalid_credentials');
        }

        return {
            accessToken: this.jwtService.sign({ userId: user.id })
        }
    }
}
