import { Body, Controller, Post } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { AuthenticationEntity } from './entity/authentication.entity';
import { LoginDto } from './dto/login.dto';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from '../users/dto/createuser.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthenticationController {
    constructor(private readonly authenticationService: AuthenticationService, private readonly usersService: UsersService) { }

    @Post('login')
    @ApiOkResponse({ type: AuthenticationEntity })
    login(@Body() { email, password }: LoginDto) {
        return this.authenticationService.login(email, password);
    }

    @Post('register')
    @ApiOkResponse({ type: AuthenticationEntity })
    async register(@Body() createUserDto: CreateUserDto) {

        const { email, password } = createUserDto;

        const user = await this.usersService.create(createUserDto);

        return this.authenticationService.login(email, password);
    }

}
