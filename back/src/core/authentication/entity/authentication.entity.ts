import { ApiProperty } from "@nestjs/swagger";

export class AuthenticationEntity {
    @ApiProperty()
    accessToken: string;
}