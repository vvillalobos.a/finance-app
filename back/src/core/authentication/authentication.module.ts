import { Module } from '@nestjs/common';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { PrismaModule } from 'src/core/prisma/prisma.module';
import { JwtStrategy } from './jwt.strategy';
import { UsersModule } from '../users/users.module';

export const JWT_SECRET = 'nj9kfsdauh7o1jid5sf62aikjol5dfsa51nlmd5vs3jo'

const TOKEN_DURATION = process.env.TOKEN_DURATION || '5m'

@Module({
    imports: [
        PrismaModule,
        PassportModule,
        JwtModule.register({
            secret: JWT_SECRET,
            signOptions: { expiresIn: TOKEN_DURATION },
        }),
        UsersModule
    ],
    controllers: [AuthenticationController],
    providers: [AuthenticationService, JwtStrategy],
})
export class AuthenticationModule { }
