import { Body, Controller, Delete, Get, Param, Post, Put, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { AccountingService } from "./accounting.service";
import { JwtAuthenticationGuard } from "src/core/authentication/jwt-authentication.guard";
import { BankAccountEntity } from "./entity/bankAccount.entity";
import { User } from "src/core/authentication/authentication.decorator";
import { UserEntity } from "src/core/users/entity/user.entity";
import { CreateBankAccountDto } from "./dto/createBankAccount.dto";
import { UpdateBankAccountBalanceDto } from "./dto/updateBankAccount.dto";
import { CreateFixedTransactionDto } from "./dto/createFixedTransaction.dto";
import { AccountBalanceDto } from "./dto/accountBalance.dto";
import { AccountBalanceEntity } from "./entity/accountBalance.entity";

@Controller('accounting')
export class AccountingController {
    constructor(private readonly accountingService: AccountingService) { }

    @Get('accounts')
    @ApiTags('Bank Accounts')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiOkResponse({ type: BankAccountEntity, isArray: true })
    getUserBankAccounts(@User() user: UserEntity) {
        return this.accountingService.bankAccounts.getUserBankAccounts(user.id);
    }

    @Post('accounts')
    @ApiTags('Bank Accounts')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiCreatedResponse({ type: BankAccountEntity })
    createNewBankAccount(@User() user: UserEntity, @Body() dto: CreateBankAccountDto) {
        return this.accountingService.bankAccounts.createNewBankAccount(user.id, dto.name, dto.currency);
    }

    @Put('accounts/:id')
    @ApiTags('Bank Accounts')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiOkResponse({ type: BankAccountEntity })
    updateBankAccountBalance(@User() user: UserEntity, @Param('id') id: string, @Body() dto: UpdateBankAccountBalanceDto) {
        return this.accountingService.bankAccounts.updateBankAccountBalance(user.id, parseInt(id), dto);
    }

    @Delete('accounts/:id')
    @ApiTags('Bank Accounts')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiOkResponse()
    deleteBankAccount(@User() user: UserEntity, @Param('id') id: number) {
        return this.accountingService.bankAccounts.deleteBankAccount(user.id, id);
    }

    @Get('accounts/:id/transactions/fixed')
    @ApiTags('Fixed Transactions')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiOkResponse()
    getAccountFixedTransactions(@User() user: UserEntity, @Param('id') id: number) {
        return this.accountingService.fixedTransactions.getUserFixedTransactions(user.id, [id])
    }

    @Post('accounts/:id/transactions/fixed')
    @ApiTags('Fixed Transactions')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiCreatedResponse({ type: BankAccountEntity })
    createNewFixedTransaction(@User() user: UserEntity, @Param('id') id: string, @Body() dto: CreateFixedTransactionDto) {
        return this.accountingService.fixedTransactions.createFixedTransaction(user.id, parseInt(id), dto);
    }

    @Get('accounts/:id/monthly-balance')
    @ApiTags('Fixed Transactions')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiCreatedResponse({ type: AccountBalanceEntity })
    getAccountMonthlyBalance(@User() user: UserEntity, @Param('id') id: string) {
        return this.accountingService.fixedTransactions.getAccountMonthlyEstimatedBalance(user.id, parseInt(id));
    }

    @Get('accounts/:id/transactions')
    @ApiTags('Transactions')
    @UseGuards(JwtAuthenticationGuard)
    @ApiBearerAuth()
    @ApiCreatedResponse({ type: AccountBalanceEntity })
    getAccountBalance(@User() user: UserEntity, @Param('id') id: string, @Body() dto: AccountBalanceDto) {
        return this.accountingService.transactions.getAccountBalance(user.id, parseInt(id), dto);
    }
}