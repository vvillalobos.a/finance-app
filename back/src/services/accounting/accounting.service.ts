import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PrismaService } from "src/core/prisma/prisma.service";
import { BankAccountEntity } from "./entity/bankAccount.entity";
import { UpdateBankAccountBalanceDto } from "./dto/updateBankAccount.dto";
import { FixedTransactionEntity } from "./entity/fixedTransaction.entity";
import { CreateFixedTransactionDto } from "./dto/createFixedTransaction.dto";
import { AccountBalanceDto } from "./dto/accountBalance.dto";
import { TransactionEntity } from "./entity/transaction.entity";
import { AccountBalanceEntity } from "./entity/accountBalance.entity";
import { AccountMonthlyEstimateBalanceEntity } from "./entity/accountMonthlyEstimatedBalance.entity";
import { CreateTransactionDto } from "./dto/createTransaction.dto";
import { CreateTransactionFromFixedDto } from "./dto/createTransactionFromFixed.dto";

@Injectable()
export class AccountingService {
    constructor(private prisma: PrismaService) { }

    utils = {
        checkIfAccountBelongsToUser: async (userId: number, accountId: number): Promise<boolean> => {
            const account = await this.prisma.bankAccount.findFirst({
                where: {
                    id: accountId,
                    ownerId: userId
                }
            });

            if (!account) {
                return false;
            }
            return true;
        }
    }

    bankAccounts = {
        getUserBankAccounts: async (userId: number): Promise<BankAccountEntity[]> => {

            const accounts = await this.prisma.bankAccount.findMany({
                where: {
                    ownerId: userId
                }
            });

            return accounts.map(account => ({
                id: account.id,
                name: account.name,
                balance: account.balance,
                currency: account.currency
            }));
        },

        createNewBankAccount: async (userId: number, name: string, currency: string): Promise<BankAccountEntity> => {
            const account = await this.prisma.bankAccount.create({
                data: {
                    name,
                    currency,
                    balance: 0,
                    ownerId: userId
                }
            });

            return {
                id: account.id,
                name: account.name,
                balance: account.balance,
                currency: account.currency
            }
        },

        updateBankAccountBalance: async (userId: number, accountId: number, dto: UpdateBankAccountBalanceDto) => {
            const account = await this.prisma.bankAccount.update({
                where: {
                    id: accountId,
                    ownerId: userId
                },
                data: {
                    balance: dto.balance,
                    name: dto.name
                }
            });

            return {
                id: account.id,
                name: account.name,
                balance: account.balance,
                currency: account.currency
            }
        },

        deleteBankAccount: async (userId: number, accountId: number) => {
            return this.prisma.bankAccount.delete({
                where: {
                    id: accountId,
                    ownerId: userId
                }
            });
        }
    }

    fixedTransactions = {

        getUserFixedTransactions: async (userId: number, accountIds?: number[]): Promise<FixedTransactionEntity[]> => {

            const request = {
                where: {
                    account: {
                        ownerId: userId
                    }
                }
            }

            if (accountIds) {
                request.where.account['id'] = {
                    in: accountIds
                }
            }

            const fixedTransactions = await this.prisma.fixedTransaction.findMany();

            return fixedTransactions.map(transaction => new FixedTransactionEntity(transaction));
        },

        createFixedTransaction: async (userId: number, accountId: number, dto: CreateFixedTransactionDto) => {

            // Check if the account belongs to the user
            const accountBelongsToUser = await this.utils.checkIfAccountBelongsToUser(userId, accountId);

            if (!accountBelongsToUser) {
                throw new UnauthorizedException('You do not have access to this account');
            }

            dto.dueDate = new Date(dto.dueDate).toISOString();

            const fixedTransaction = await this.prisma.fixedTransaction.create({
                data: {
                    ...dto,
                    account: {
                        connect: {
                            id: accountId
                        }
                    }
                }
            });

            return new FixedTransactionEntity(fixedTransaction);
        },

        removeFixedTransaction: async (userId: number, transactionId: number) => {
            return this.prisma.fixedTransaction.delete({
                where: {
                    id: transactionId,
                    account: {
                        ownerId: userId
                    }
                }
            });
        },

        getAccountMonthlyEstimatedBalance: async (userId: number, accountId: number): Promise<AccountMonthlyEstimateBalanceEntity> => {

            // Get all fixed transactions for the account and calculate the estimated balance
            // This is not bound by date, it's supposed to be the current state of the account.


            const account = await this.prisma.bankAccount.findFirst({
                where: {
                    id: accountId,
                    ownerId: userId
                }
            });

            if (!account) {
                throw new UnauthorizedException('You do not have access to this account');
            }

            const fixedTransactions = await this.prisma.fixedTransaction.findMany({
                where: {
                    accountId
                }
            });

            const transactions = fixedTransactions.map(transaction => new FixedTransactionEntity(transaction));

            const balance = transactions.reduce((acc, transaction) => acc + transaction.amount, 0);
            const income = transactions.filter(transaction => transaction.amount > 0).reduce((acc, transaction) => acc + transaction.amount, 0);
            const expenses = Math.abs(transactions.filter(transaction => transaction.amount < 0).reduce((acc, transaction) => acc + transaction.amount, 0));

            return new AccountMonthlyEstimateBalanceEntity({
                account: {
                    id: account.id,
                    name: account.name,
                    balance: account.balance,
                    currency: account.currency
                },
                balance,
                income,
                expenses,
                transactions
            });

        }

        // No update method since fixed transactions are supposed to be immutable
    }

    transactions = {
        // list, get, create, delete
        getAccountBalance: async (userId: number, accountId: number, dto: AccountBalanceDto): Promise<AccountBalanceEntity> => {
            const account = await this.prisma.bankAccount.findFirst({
                where: {
                    id: accountId,
                    ownerId: userId
                }
            });

            if (!account) {
                throw new UnauthorizedException('You do not have access to this account');
            }

            // Get all transactions for the account in the date range
            const startDate = new Date(dto.startDate).toISOString();
            const endDate = new Date(dto.endDate || Date.now()).toISOString();

            const transactionsResult = await this.prisma.transaction.findMany({
                where: {
                    accountId,
                    date: {
                        gte: startDate,
                        lte: endDate
                    }
                }
            });

            const transactions = transactionsResult.map(transaction => new TransactionEntity(transaction));

            return new AccountBalanceEntity({
                account: {
                    id: account.id,
                    name: account.name,
                    balance: account.balance,
                    currency: account.currency
                },
                balance: account.balance,
                income: transactions.filter(transaction => transaction.amount > 0).reduce((acc, transaction) => acc + transaction.amount, 0),
                expenses: transactions.filter(transaction => transaction.amount < 0).reduce((acc, transaction) => acc + transaction.amount, 0),
                transactions
            });
        },
        createTransaction: async (userId: number, accountId: number, dto: CreateTransactionDto): Promise<TransactionEntity> => {
            // Check if the account belongs to the user
            const accountBelongsToUser = await this.utils.checkIfAccountBelongsToUser(userId, accountId);

            if (!accountBelongsToUser) {
                throw new UnauthorizedException('You do not have access to this account');
            }

            dto.date = new Date(dto.date).toISOString();

            const transaction = await this.prisma.transaction.create({
                data: {
                    ...dto,
                    account: {
                        connect: {
                            id: accountId
                        }
                    }
                }
            });

            return new TransactionEntity(transaction);
        },
        createTransactionFromFixed: async (userId: number, accountId: number, transactionId: number, dto: CreateTransactionFromFixedDto) => {
            const fixedTransaction = await this.prisma.fixedTransaction.findFirst({
                where: {
                    id: transactionId,
                    account: {
                        ownerId: userId
                    }
                }
            });

            if (!fixedTransaction) {
                throw new UnauthorizedException('You do not have access to this fixed transaction');
            }

            const transaction = await this.prisma.transaction.create({
                data: {
                    title: fixedTransaction.title,
                    amount: dto.amount,
                    date: dto.date,
                    account: {
                        connect: {
                            id: accountId
                        }
                    }
                }
            });

            return new TransactionEntity(transaction);
        }
    }

    cards = {}

}