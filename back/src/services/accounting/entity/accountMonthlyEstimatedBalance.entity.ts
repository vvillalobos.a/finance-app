import { ApiProperty } from "@nestjs/swagger";
import { BankAccountEntity } from "./bankAccount.entity";
import { FixedTransactionEntity } from "./fixedTransaction.entity";

export class AccountMonthlyEstimateBalanceEntity {
    constructor(partial: Partial<AccountMonthlyEstimateBalanceEntity>) {
        Object.assign(this, partial);
        // Remove the createdAt and updatedAt fields from the response
        delete this.createdAt;
        delete this.updatedAt;
    }

    @ApiProperty()
    public account: BankAccountEntity;
    @ApiProperty()
    public balance: number;
    @ApiProperty()
    public income: number;
    @ApiProperty()
    public expenses: number;
    @ApiProperty()
    public transactions: FixedTransactionEntity[];

    private createdAt?: Date;
    private updatedAt?: Date;
}