import { ApiProperty } from "@nestjs/swagger";

export class TransactionEntity {
    constructor(partial: Partial<TransactionEntity>) {
        Object.assign(this, partial);
        // Remove the createdAt and updatedAt fields from the response
        delete this.createdAt;
        delete this.updatedAt;
    }

    @ApiProperty()
    public id: number;
    @ApiProperty()
    public title: string;
    @ApiProperty()
    public amount: number;
    @ApiProperty()
    public date: Date;

    @ApiProperty()
    public accountId: number;
    @ApiProperty()
    public fixedTransactionId?: number;

    private createdAt?: Date;
    private updatedAt?: Date;
}