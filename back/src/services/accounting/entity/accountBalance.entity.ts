import { ApiProperty } from "@nestjs/swagger";
import { BankAccountEntity } from "./bankAccount.entity";
import { TransactionEntity } from "./transaction.entity";

export class AccountBalanceEntity {
    constructor(partial: Partial<AccountBalanceEntity>) {
        Object.assign(this, partial);
        // Remove the createdAt and updatedAt fields from the response
        delete this.createdAt;
        delete this.updatedAt;
    }

    @ApiProperty()
    public account: BankAccountEntity;
    @ApiProperty()
    public balance: number;
    @ApiProperty()
    public income: number;
    @ApiProperty()
    public expenses: number;
    @ApiProperty()
    public transactions: TransactionEntity[];

    private createdAt?: Date;
    private updatedAt?: Date;

} 