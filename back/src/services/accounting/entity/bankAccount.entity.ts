import { ApiProperty } from "@nestjs/swagger"

export class BankAccountEntity {

    constructor(partial: Partial<BankAccountEntity>) {
        Object.assign(this, partial)

        // Remove the createdAt and updatedAt fields from the response
        delete this.createdAt
        delete this.updatedAt
    }

    @ApiProperty()
    public id: number
    @ApiProperty()
    public name: string
    @ApiProperty()
    public balance: number
    @ApiProperty()
    public currency: string

    private createdAt?: Date
    private updatedAt?: Date
}