import { ApiProperty } from "@nestjs/swagger";
import { BankAccountEntity } from "./bankAccount.entity";
import { FixedTransaction } from "@prisma/client";

export class FixedTransactionEntity {

    constructor(partial: Partial<FixedTransactionEntity>) {
        Object.assign(this, partial);

        // Remove the createdAt and updatedAt fields from the response
        delete this.createdAt;
        delete this.updatedAt;
    }

    @ApiProperty()
    public id: number

    @ApiProperty()
    public title: string

    @ApiProperty()
    public description: string

    @ApiProperty()
    public amount: number

    @ApiProperty()
    public dueDate: Date

    @ApiProperty()
    public accountId: number

    private createdAt?: Date
    private updatedAt?: Date

    @ApiProperty()
    account?: BankAccountEntity
}