import { ApiProperty } from "@nestjs/swagger";
import { IsIn, IsNotEmpty, IsString } from "class-validator";

// TODO: Expand the list of currencies allowed
export const CURRENCIES_ALLOWED = ['CLP', 'USD'];

export class CreateBankAccountDto {

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    name: string;

    @ApiProperty()
    @IsIn(CURRENCIES_ALLOWED)
    currency: string;
}