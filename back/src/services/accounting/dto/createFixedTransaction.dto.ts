import { ApiProperty } from "@nestjs/swagger";
import { IsDateString, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class CreateFixedTransactionDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    title: string;

    @IsString()
    @ApiProperty()
    description: string;

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty()
    amount: number;

    @IsNotEmpty()
    @IsDateString({
        strict: true
    }) // YYYY-MM-DD
    @ApiProperty()
    dueDate: string;
}