import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import { IsDateString } from "class-validator";

export class AccountBalanceDto {
    @IsDateString({
        strict: true
    })
    @ApiProperty()
    startDate: string;

    @IsDateString({
        strict: true
    })
    @ApiProperty({ required: false })
    endDate?: string; // If not provided, it will be the current date
}