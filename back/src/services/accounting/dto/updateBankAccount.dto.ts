import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class UpdateBankAccountBalanceDto {
    @ApiProperty({ required: false })
    @IsString()
    @IsNotEmpty()
    name?: string;

    @ApiProperty({ required: false })
    @IsNumber()
    balance?: number;
}