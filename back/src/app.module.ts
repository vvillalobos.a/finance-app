import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthenticationModule } from './core/authentication/authentication.module';
import { AccountingModule } from './services/accounting/accounting.module';

@Module({
  imports: [AuthenticationModule, AccountingModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
