---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: Fullstack application designed to help people understand their personal finances. This is a demonstrative project to showcase some of the authors capabilities.
actionText: Quick Start →
actionLink: /guide/
features:
- title: Feature 1 Title
  details: Feature 1 Description
- title: Feature 2 Title
  details: Feature 2 Description
- title: Feature 3 Title
  details: Feature 3 Description
footer: Made by Víctor Villalobos with ❤️
---
