# Requirements

## About the project 
This project's goal is to demonstrate knowledge of the conceptualization, design, and implementation of a full stack web application.  

This includes documentation, database design, API design, front-end design, and a CI/CD pipeline for continuous deployment all in one repository.

## Client Requirements
The client has requested an application that can be executed on their devices, such as smartphone and laptop computer. The application has to help them to manage their personal finances and to keep track of their expenses. The client has also requested that the application be user-friendly and easy to use.

### 1. Transactions
The application should allow the user to add, edit, and delete transactions. Each transaction should have a description, amount, and date. The transactions will then be shown in 3 main ways: A list of all transactions, a chart of all transactions and balance, and cards with te sum.

### 2. Bank account balance
The application should allow the user to add, edit, and delete bank accounts. Each bank account should have a name, balance, and currency. The bank accounts will then be shown in a list, and if the user is thorough, the balances in-app should be the same as the balances in the bank.

#### 2.1. Cards
Each bank account can have one or multiple cards. Each card should have a name, number, and balance. The cards will then be shown in a list, and if the user is thorough, the balances in-app should be the same as the balances in the bank.