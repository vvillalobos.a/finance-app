init:
	cp back/env.template back/.env && cp front/env.template front/.env

up:
	docker-compose up -d

migrate:
	docker-compose exec nest yarn prisma migrate dev

seed:
	docker-compose exec nest yarn prisma db seed