# Finance App

This is a finance app that allows users to track their expenses and income. It is built using NestJS, Postgres and Vue 3.  

This app is made for my personal use and is not yet ready for deployment. It is still in development and may not be stable.

This is mainly a demo project to showcase my skills in full stack development. I am open to feedback and suggestions on how to improve the app.

### Features
- Add, edit and delete bank accounts
- View, add and delete transactions
- View transaction history
- View monthly fixed income and expenses

### Planned Features
- Set up savings goals
- View savings progress
- Give insights on spending habits
- Set up savings plans

## Development

### Dev Environment Setup

#### Requirements:
- Docker & Docker Compose
- Make
- Ideally MacOS or Linux (Windows users may need to use WSL2 or a VM)

#### Setup
1. Clone the repository
2. Run `make init` to set up the environment. This will create the .env files needed for the services. **This will overwrite existing .env files if any.**
3. Run `make up` to start all the services and start.
4. Run `make migrate` to create tables and seed the database with some initial data for testing.
5. Access the app's user interface at `http://localhost:5173`

You can check each service's README for more information on how to run them individually.  

### Services
These services are run using Docker and Docker Compose. Each service has its own directory and README file with more information on how to run them individually. Please make sure you are not using any of the ports listed below before running the services.

| Service | Description | Port | Directory |
| --- | --- | --- | --- |
| Postgres | Database | 5432 | - |
| PgAdmin | Database Manager | 8080 | - |
| Vuejs | User Interface | 5173 | front |
| NestJS | Backend / API | 4000 | back |
| VuePress | Documentation | - | docs |

### Database
The database is managed using Prisma within NestJS and comes with a seed you may need to use to initialize the database. More instructions on how to manage the database can be found in the `back` directory.

### Deployment
The app is not yet ready for deployment. More information will be provided once the app is ready for deployment.

Currently the app is only available for local development.

### Contributing
Right now this repository is not open for direct contributions as this is just a demo and has no real use. However, I am open to feedback and suggestions on how to improve the app. Feel free to open an issue or contact me directly. 

With this said, if you would like to use this app for your own personal use, feel free to fork the repository and make your own changes.

